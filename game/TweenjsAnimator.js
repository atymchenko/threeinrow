/**
 * Created by ader on 2/15/15.
 */

this.TweenjsAimator = function(target) {

    if ( !(target instanceof IAnimatable) ) {
        throw new Error("Animation target should implement IAnimatable");
    }

    var chain;

    this.animate = function(target) {

        createjs.TweenJS.get(target)
            .to({x:300}, 1000).to({x:0}, 0)
            .call(onAnimationComplete);

    }

    this.animateTo = function(target, x, y) {

    }

    /**
     * @param item
     */
    this.animateEffect = function(parent, effect) {
        parent.addEffect(effect);
    }


    var getTweenChain = function(tween) {

        var chain = new Chain();

        tween.call(chain.resolve);

        return chain;
    }

    var getEffectChain = function() {

    }

}
this.TweenjsAimator.prototype = new Animator();
