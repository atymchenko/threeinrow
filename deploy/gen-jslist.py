from os import walk
from os import sep

rootFolder = ".."
outputFileName = "app.js"
outputFolder = ".."

def scan():
    list = []
    for (dirpath, dirnames, filenames) in walk(rootFolder):
        if filenames and len(filenames) > 0:
            for fileName in filenames:
                if fileName.endswith('.js'):
                    list.append( dirpath + sep + fileName )

    return list

def gen_list(names):
    file = open(outputFolder + sep + outputFileName, 'w')

    start = '''requirejs(['''

    end = '''],
    function   () {
		onLoad();
    });'''

    list = []
    for fileName in names:
        list.append( '"'+fileName.replace('\\', '/')+'"' )

    start += ',\n'.join(list) + end
    file.write(start)
    file.close()
    return

gen_list(scan())
