/**
 * Created by ader on 2/8/15.
 */

this.CanvasDrawUtils = function(ctx) {

    var self = this;

    this.drawItem = function (item) {
        var indent = 10;
        ctx.fillStyle = styles[item.getType()-1];
        ctx.fillRect(item.getX()*square + indent, item.getY()*square + indent, square - 2*indent, square - 2*indent);
    }

    this.drawEmpty = function (x,y) {
        var indent = 1;
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.fillRect(x*square + indent, y*square + indent, square - 2*indent, square - 2*indent);
    }

    this.drawAddedToMatch = function (item) {
        var x = item.getX();
        var y = item.getY();
        var indent = 1;
        ctx.fillStyle = "rgb(0,255,0)";
        ctx.fillRect(x*square + indent, y*square + indent, square - 2*indent, square - 2*indent);
        self.drawItem(item);
    }

}
