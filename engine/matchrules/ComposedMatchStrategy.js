/**
 * Created by ader on 1/30/15.
 */

this.ComposedMatchStrategy = function() {

    this.match = function(item1, item2) {
        return (new TypeMatchStrategy()).match(item1, item2) && [
            new HorizontalMatchStrategy(),
            new VerticalMatchStrategy(),
            new DiagonalMatchStrategy()
        ].some(function(rule){
                return rule.match(item1, item2);
            });
    }

}
