/**
 * Created by ader on 1/30/15.
 */

this.HorizontalMatchStrategy = function() {

    this.match = function(item1, item2) {
        return Utils.prototype.isHorizontalNeighbours(item1, item2);
    }

}
