/**
 * Created by ader on 1/30/15.
 */

this.TypeMatchStrategy = function() {

    this.match = function(item1, item2) {

        return item1.getType() === item2.getType();

    }

}
