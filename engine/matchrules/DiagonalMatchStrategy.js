/**
 * Created by ader on 1/30/15.
 */

this.DiagonalMatchStrategy = function() {

    this.match = function(item1, item2) {
        return Utils.prototype.isDiagonalNeighbours(item1, item2);
    }

}
