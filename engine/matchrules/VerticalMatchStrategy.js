/**
 * Created by ader on 1/30/15.
 */

this.VerticalMatchStrategy = function() {

    this.match = function(item1, item2) {
        return Utils.prototype.isVerticalNeighbours(item1, item2);
    }

}