/**
 * Created by ader on 1/28/15.
 */

this.EngineModel = function(width, height, generator) {

    var self = this;

    var stage = new StageModel(width, height);

    /**
     * @type {IMatchStrategy}
     */
    var matchStrategy = new ComposedMatchStrategy();

    /**
     * @type {IMoveStrategy}
     */
    var moveStrategy = new MoveDownStrategy(stage);

    var fillStrategy = new FillTopStrategy(stage, generator);

    var match = [];

    this.getStage = function() {
        return stage;
    }

    /**
     * Return Matrix of items to be setted on the stage
     * return next bunch of items
     */
    this.getNextBunch = function() {}

    /**
     * Help users to find new match
     * return suggested items
     */
    this.makeSuggestion = function() {}

    /**
     * return bunch of matched items or null
     */
    this.getMatch = function() {
        return match;
    }

    this.tryAddItemToMatch = function(item) {

        var len = match.length;

        if (len == 0) {
            match.push(item);
            return true;
        }

        if (!item || self.checkInMatch(item)) {
            return false;
        }

        if (matchStrategy.match(match[len-1], item)) {
            match.push(item);
            return true;
        }

        return false;
    }

    this.checkInMatch = function(item) {
        return match.indexOf(item) >= 0;
    }

    this.undoMatchItem = function() {
        return match.pop();
    }

    var indexes = [];
    this.finishMatch = function() {

        match.forEach(function(item){
            var x = item.getX();
            if (indexes.indexOf(x) < 0) {
                indexes.push(x);
            }
        });

        match.forEach(function(item){stage.removeItem(item)});
        match = [];
    }

    this.moveItems = function() {
        moveStrategy.move(indexes);
        indexes = [];
    }

    this.fillItems = function() {
        fillStrategy.fill();
    }


}
