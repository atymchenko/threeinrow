/**
 * Created by ader on 1/29/15.
 */

this.StageModel = function(width, height) {

    var self = this;

    var matrix = new Matrix(height, width);

    /**
     * Matrix of stage
     * @returns Matrix
     */
    this.getMatrix = function() {
        return matrix;
    }

    this.hasItem = function(x ,y) {
        return matrix.has(y, x);
    }

    this.getItem = function(x, y) {
        return matrix.get(y, x);
    }

    this.removeItem = function(item) {
        matrix.remove(item.getY(), item.getX());
        item.setX(NaN);
        item.setY(NaN);
    }

    this.addItem = function(item, x, y) {
        matrix.add(y, x, item);
        item.setX(x);
        item.setY(y);
    }

    /**
     * Swaps location of two items
     * @param item1
     * @param item2
     */
    this.swapItems = function(item1, item2) {
        var x1 = item1.getX();
        var y1 = item1.getY();
        var x2 = item2.getX();
        var y2 = item2.getY();
        self.addItem(item1, x2, y2);
        self.addItem(item2, x1, y1);
    }

    /**
     * Move item to location (destX,destY)
     * @param item
     * @param destX
     * @param destY
     */
    this.moveItem = function(item, destX, destY) {
        self.removeItem(item);
        self.addItem(item, destX, destY);
    }

    /**
     *
     * @param item1
     * @param item2
     * @returns {{x: number, y: number}}
     */
    this.getDistanceBetweenItems = function(item1, item2) {
        return {x: item2.getX() - item1.getX(), y: item2.getY() - item1.getY()};
    }


}
