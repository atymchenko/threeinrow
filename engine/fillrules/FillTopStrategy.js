/**
 * Created by ader on 2/7/15.
 */

/**
 *
 * @param stage
 * @param generator
 * @constructor
 */
this.FillTopStrategy = function(stage, generator) {

    this.fill = function() {

        var columnItems;
        for (var i = 0, length = stage.getMatrix().getSize().columns; i < length; i++) {
            columnItems = stage.getMatrix().getColumnItems(i);

            columnItems.forEach(function(item, index){

                var x = i;
                var y = index;

                if (!stage.hasItem(x, y)) {
                    stage.addItem(generator.generate(x,y), x, y);
                }

            });

        }

        return true;

    }

}
