/**
 * Created by ader on 1/28/15.
 */

this. ItemModel = function(type, x, y) {

    var posX = x || NaN;
    var posY = y || NaN;

    /**
     * Simple indentifier of type
     * e.g. May be a color of item, whatever
     * @returns int
     */
    this.getType = function() {
        return type;
    }

    this.setX = function(x) {
        posX = x;
    }

    this.getX = function() {
        return posX;
    }

    this.setY = function(y) {
        posY = y;
    }

    this.getY = function() {
        return posY;
    }


}
