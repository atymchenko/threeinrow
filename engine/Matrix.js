/**
 * Created by ader on 1/29/15.
 */

/**
 *
 * Array of arrays
 * e.g. [[row1], [row2], [row3]]
 * @constructor
 */
this.Matrix = function(rows, columns) {

    var source;
    var empty = null;

    var init = (function() {
        source = [];
        source.length = rows;

        for (var r = 0; r < rows; r++) {
            var row = [];
            row.length = columns;
            for (var c = 0; c < columns; c++) {
                row[c] = empty;
            }
            source[r] = row;
        }
    })();


    this.getSource = function() {
        return source;
    }

    /**
     * Size of matrix Rows x Columns
     * @returns {{columns: (Number|number), rows: (Number|number)}}
     */
    this.getSize = function() {
        return {rows: rows, columns: columns};
    }

    this.has = function(row, column) {
        return source[row][column] !== empty;
    }

    this.get = function(row, column) {
        return source[row][column];
    }

    this.add = function(row, column, item) {
        source[row][column] = item;
    }

    this.remove = function(row, column) {
        source[row][column] = empty;
    }

    this.getRowItems = function(row) {
        return source[row];
    }

    this.getColumnItems = function(column) {
        return source.map(function(rowItems){return rowItems[column]})
    }

    this.reset = function() {
        init();
    }

}
