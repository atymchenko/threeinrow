/**
 * Created by ader on 1/30/15.
 */

this.Utils = function(){}

Utils.prototype.isHorizontalNeighbours = function(item1, item2) {
    return item1.getY() === item2.getY() && Math.abs(item2.getX() - item1.getX()) === 1;
}

Utils.prototype.isVerticalNeighbours = function(item1, item2) {
    return item1.getX() === item2.getX() && Math.abs(item2.getY() - item1.getY()) === 1;
}

Utils.prototype.isDiagonalNeighbours = function(item1, item2) {
    return Math.abs(item2.getX() - item1.getX()) === 1 && Math.abs(item2.getY() - item1.getY()) === 1;
}
