/**
 * Created by ader on 2/1/15.
 */

this.MoveDownStrategy = function(stage) {

    this.move = function(indexes) {

        var moveDown = function(item) {
            var x = item.getX();
            var y = item.getY();

            if (y < columnItems.length-1) {
                var isNext = stage.hasItem(x, y+1);

                if (!isNext) {
                    stage.moveItem(item, x, y+1);
                    moveDown(item);
                }
            }

        }

        var columnItems;

        for (var j = 0; j < indexes.length; j++) {
            var index = indexes[j];
            columnItems = stage.getMatrix().getColumnItems(index);

            // exclude last item
            var maxI = columnItems.length-2;
            for (var i = maxI; i >= 0; i--) {
                var item = columnItems[i];

                if (!item) {
                    continue;
                }

                moveDown(item);

            }

        }


        return true;

    }

}
