/**
 * Created by ader on 2/7/15.
 */

this.Play = function(model) {

    var status = "uninit";

    this.initGame = function() {
        //model.fillItems();
        status = "init";
    }

    this.startRound = function() {
        status = "start";
        model.fillItems();
        status = "game";
    }

    this.endRound = function() {
        model.finishMatch();
        model.moveItems();
        status = "end";
    }

    this.addItem = function(item) {
        if (status == "game") {
            return model.tryAddItemToMatch(item);
        }
        return false;
    }

    this.checkItem = function(item) {
        return model.checkInMatch(item);
    }

    this.removeItem = function(item) {
        if (status == "game") {
            var match = model.getMatch();
            if (match[match.length-1] === item)
                return model.undoMatchItem();
        }
        return false;
    }

}
